# Post title
---

Intro

# First section

Hi.<sup id="p4_obs">[1](#P4_OBS)</sup>.
[this is a link] that should work.

# Conclusion

-----

<b id="P4_OBS">1</b> Come here often?.[↩](#p4_obs)

----

[this is a link]: https://en.wikipedia.org/wiki/Topological_sorting
