#About me
----

![myself](profile_pic.jpg)

Hello!

I'm a computer scientist and software engineer living in Toronto, Canada.

My daily job brings me in close contact with software based on the LLVM
project; as such, any compiler-related topics are of interest to me and
you might see me around the LLVM-developers conference. In the April 2019
conference, I had the chance to present [a tutorial] on LLVM's Intermediate
Representation.

As shown by the tutorial above, I enjoy the challenge of creating clean
explanations for concepts. Effective communication is something I want to get
get better at and it is the biggest reason for this blog to exist. Combined
with a passion for helping others understand basic concepts and get better at
them, it leads me write to a lot of "introduction to X" type of posts - that's
most of the contents of this blog. It's not purely for altruistic reasons
though, it helps *me* learn too.

During my undergrad, I had the chance to compete in [ACM's ICPC], a programming
contest focused on algorithms. The skill ceiling in that competition is very
high, and it's an area I've always wanted to get better at; on my journey to
get there, I'll write posts about the interesting problems I come across.

My [undergrad thesis] was related to the memory access pattern and energy
efficiency of certain cryptographic algorithms. During my masters, I ventured
into the theory of distributed systems, in the Computer Science department of
the University of Toronto. The thesis was [published here].

I should mention that I quite enjoy C++ and CMake.

----

You can contact me [@fpiovezan] on Twitter, or on [LinkedIn].

----

[a tutorial]: https://www.youtube.com/watch?v=m8G_S5LwlTo
[ACM's ICPC]: https://icpc.baylor.edu/
[undergrad thesis]: https://ieeexplore.ieee.org/document/7724052
[@fpiovezan]: https://twitter.com/fpiovezan
[LinkedIn]: https://www.linkedin.com/in/felipe-de-azevedo-piovezan-a1106ab9/
[published here]: https://drops.dagstuhl.de/opus/volltexte/2020/11802/
