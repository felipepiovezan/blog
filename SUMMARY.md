# Summary

* [About me](README.md)
* [2019/11/17/ Topological sorting - Improve spam](programming_challenges/improve_spam/README.md)
* [2020/01/27/ Build systems 101 part 1](build_system_basics/README.md)
* [2020/02/22/ Build systems 101 part 2](build_system_p2/README.md)
* [2020/03/06/ On static variables, static storage, static initialization, constant initialization, constinit, constexpr](const_constinit_constexpr_consteval/README.md)
* [2020/05/03/ A Git introduction with no commands](git_basics/README.md)
